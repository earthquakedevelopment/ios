//
//  AddHouseFooterViewCell.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 06.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class AddHouseFooterViewCell: UITableViewCell {
    
    weak var view:EDMyObjectsViewController!

    @IBOutlet weak var addHouseButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        addHouseButton.clipsToBounds = true
        addHouseButton.layer.cornerRadius = 4.0
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func seeAllHousesAction(_ sender: AnyObject) {
        
        view.performSegue(withIdentifier: "seeAllHouses", sender: nil)
        
    }
    @IBAction func addHouseAction(_ sender: AnyObject) {
        
        view.performSegue(withIdentifier: "addHouse", sender: nil)
    }
}
