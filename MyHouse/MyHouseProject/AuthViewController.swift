//
//  AuthViewController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 17.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftValidator

class AuthViewController: UIViewController, UITextFieldDelegate, ValidationDelegate {
    
    var presenter:EDAuthPresenter! = nil
    let validator = Validator()
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var whiteBackgroundView: UIView!
    
    @IBOutlet weak var errorPasswordError: UILabel!
    @IBOutlet weak var errorLoginLabel: UILabel!
    
    @IBOutlet weak var LoginTextField: UITextField!
    @IBOutlet weak var PassTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whiteBackgroundView.layer.cornerRadius = 3.0
        loginButton.layer.cornerRadius = 3.0
        
        LoginTextField.layer.cornerRadius = 3.0
        LoginTextField.clipsToBounds = true
        
        PassTextField.layer.cornerRadius = 3.0
        PassTextField.clipsToBounds = true
        
        presenter = EDAuthPresenter.init(view: self)
        
        validator.registerField(LoginTextField, errorLabel: errorLoginLabel, rules: [RequiredRule.init(message: "Введите логин")])
        validator.registerField(PassTextField, errorLabel: errorPasswordError   , rules: [RequiredRule.init(message: "Введите пароль")])
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: OperationQueue.init()) { [weak self] (notification) in
            
            OperationQueue.main.addOperation({ 
                
                UIView.animate(withDuration: 0.35, animations: {
                    
                    self?.logoImageView.alpha = 0
                    
                })
                
            })
            
            
            
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: OperationQueue.init()) { [weak self] (notification) in
            
            OperationQueue.main.addOperation({
                
                UIView.animate(withDuration: 0.5, animations: {
                    
                    self?.logoImageView.alpha = 1
                    
                })
                
            })
            
        }

    }
    
    @IBAction func JoinButton(_ sender: AnyObject) {
        
        validator.validate(self)
        
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.init(red: 52.0/255.0, green: 168.0/255.0, blue: 83.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.borderWidth = 0
        
    }
    
    func validationSuccessful() {
        
        
        presenter.authUserByPhone(LoginTextField.text, password: PassTextField.text);
        
        
        errorPasswordError.isHidden = true
        errorLoginLabel.isHidden = true
        
        PassTextField.layer.borderWidth = 0
        LoginTextField.layer.borderWidth = 0

    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
        errorPasswordError.isHidden = true
        errorLoginLabel.isHidden = true
        
        PassTextField.layer.borderWidth = 0
        LoginTextField.layer.borderWidth = 0
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false

        }
        
    }
    
    
}
