//
//  ChangeObjectTableViewController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 13.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit


class ChangeObjectTableViewController: UITableViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "changeCell", for: indexPath)
            return cell
            
        } else if (indexPath as NSIndexPath).row == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "deleteCell", for: indexPath) 
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).row == 0{
            return 259
        } else if (indexPath as NSIndexPath).row == 1{
            return 310
        } else {
            return 44
        }
    }
}
