//
//  ConfirmRegistrationController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 26.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import Foundation
import UIKit
import SwiftSpinner
import SwiftValidator

class ConfirmRegistrationViewController: UIViewController, UITextFieldDelegate, ValidationDelegate {
    
    var presenter:EDConfirmRegistrationPresenter! = nil
    let validator = Validator()
    
    @IBOutlet weak var whiteBackgroundView: UIView!
    @IBOutlet weak var ConfirmTextField: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var errorCodeLabel: UILabel!
    @IBOutlet weak var CodeField: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whiteBackgroundView.layer.cornerRadius = 3.0
        confirmButton.layer.cornerRadius = 3.0
        
        CodeField.layer.cornerRadius = 3.0
        CodeField.clipsToBounds = true
        
        presenter = EDConfirmRegistrationPresenter.init(view: self)
        
        phoneLabel.text = UserPhone
        
        validator.registerField(CodeField, errorLabel: errorCodeLabel, rules: [RequiredRule.init(message: "Введите код")])
        
//        ConfirmTextField.text! = "СМС с кодом подтверждения было отправленно на номер:\n"+UserPhone
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    
    @IBAction func ConfirmRegistrationButton(_ sender: AnyObject) {

        validator.validate(self)
        
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.init(red: 52.0/255.0, green: 168.0/255.0, blue: 83.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.borderWidth = 0
        
    }
    
    func validationSuccessful() {
        
        self.presenter.checkRegistrationCode(CodeField.text!)
        
        errorCodeLabel.isHidden = true

        CodeField.layer.borderWidth = 0

        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
        errorCodeLabel.isHidden = true
        
        CodeField.layer.borderWidth = 0

        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
            
        }
        
    }


}

