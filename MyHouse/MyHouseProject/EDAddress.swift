//
//  EDAddress.swift
//  MyHouseProject
//
//  Created by Павел on 02.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDAddress: NSObject {
    
    var city: EDCity!
        {
        didSet {
            
            street = nil
            house = nil
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDAddressCityWasChanged"), object: self)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDAddressStreetWasChanged"), object: self)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDAddressHouseWasChanged"), object: self)
        }
    }
    var street: EDStreet!{
        didSet {
            
            house = nil
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDAddressStreetWasChanged"), object: self)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDAddressHouseWasChanged"), object: self)
            
        }
    }
    var house: EDHouse!
        {
        didSet {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDAddressHouseWasChanged"), object: self)
            
        }
    }
    
    internal func isCorrect () -> Bool {
        
        return house != nil && street != nil && house != nil
        
    }
    
    internal func getFullAddress () -> String {
        
        if isCorrect() {
            
            return city.name + " " + street.socr + " " + street.name + " " + house.name
            
        } else {
        
            return "Некорректный адрес"
        }
        
    }
}
