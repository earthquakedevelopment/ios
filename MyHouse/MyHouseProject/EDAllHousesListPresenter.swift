//
//  EDAllHousesListPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 14.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDAllHousesListPresenter: NSObject {
    
    weak var view: EDAllHousesListViewController?;
    
    var houses: NSMutableArray = NSMutableArray()
    
    var currentPage:Int = 0
    
    var searchString:String = ""
    
    var currentCity:EDCity!
    
    var townsList:NSMutableArray = []
    
    var lastPage:Bool = false

    init(view: EDAllHousesListViewController!) {
        
        self.view = view
        
        currentCity = EDCity(object: UserDefaults.standard.object(forKey: "currentCity") as! NSDictionary)
        
        super.init()
        
    }
    
    internal func updateHouses(_ sString: String) {
        
        lastPage = false
        
        SwiftSpinner.show("")

        houses.removeAllObjects()
        
        view?.tableView.reloadData()
        
        searchString = sString
        
        currentPage = 0;

        loadHousesInCity(currentCity, page: currentPage, searchString: searchString)
    
    }
    
    internal func nextPage() {
        
        if lastPage == true {
            return
        }
        
        currentPage+=1
        
        loadHousesInCity(currentCity, page: currentPage, searchString: searchString)
        
    }
    
    func loadHousesInCity(_ city: EDCity, page: Int, searchString: String) {
        

        
        EDHousesManager.sharedInstance.searchHouses(String(city.id), searchString: searchString, page: page, success: { (houses, lastPage) in
            
            for object in houses {
                
                let house: NSDictionary = object as! NSDictionary
                
                self.houses.add(EDHouseObject(object: house))
                
            }
            
            self.lastPage = lastPage
            
            self.view?.tableView.reloadData()

            SwiftSpinner.hide()
            
            }) { (error) in

                SwiftSpinner.hide()
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }

                
        }
        
    }
    
    internal func updateCitiesList () {
        
        SwiftSpinner.show("")
        
        EDHousesManager.sharedInstance.updateCities({ (json) in
            
            self.townsList.removeAllObjects()
            
            for object in json {
                
                let city: NSDictionary = object as! NSDictionary
                
                self.townsList.add(EDCity(object: city))
                
            }
            
            self.view?.cityPicker.reloadAllComponents()
            
            SwiftSpinner.hide()
            
            }) { (error) in
                
                SwiftSpinner.hide()
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка!", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        alert.dismiss(animated: true, completion: nil)
                        
                    }))
                    
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }
                
        }
        
        
        
    }
    
}
