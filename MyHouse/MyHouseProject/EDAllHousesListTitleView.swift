//
//  EDAllHousesListTitleView.swift
//  MyHouseProject
//
//  Created by Павел on 14.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDAllHousesListTitleView: UIView, UITextFieldDelegate {

    weak var presenter:EDAllHousesListPresenter!
    @IBOutlet weak var titleTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let rec:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EDAllHousesListTitleView.beginEditing))
        // Initialization code
        addGestureRecognizer(rec)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        presenter.updateHouses("")
        
    }
    
    func beginEditing () {
        
        titleTextField.becomeFirstResponder()
        
    }


}
