//
//  EDAllHousesListViewController.swift
//  MyHouseProject
//
//  Created by Павел on 14.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftSVG

class EDAllHousesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {

    var presenter:EDAllHousesListPresenter! = nil
    
    var cityPicker:UIPickerView = UIPickerView()
    
    let searchController = UISearchController(searchResultsController:  nil)
    
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {

        cityPicker.dataSource = self
        
        cityPicker.delegate = self
        
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.barTintColor = UIColor(hexString: "3B454D")
        self.searchController.searchBar.tintColor = UIColor.white
        self.searchController.searchBar.alpha = 1.0
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false

        self.definesPresentationContext = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = EDAllHousesListPresenter.init(view: self)
        
        presenter.updateHouses("")
        
        presenter.updateCitiesList()

        navigationItem.title = "Дома " + presenter.currentCity.socr + " " + presenter.currentCity.name
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return presenter.houses.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let house:EDHouseObject = presenter.houses[(indexPath as NSIndexPath).row] as! EDHouseObject
        
        if house.name.characters.count > 0 {
            
            let cell:HouseListViewCell = tableView.dequeueReusableCell(withIdentifier: "mainCell") as! HouseListViewCell
            
            cell.setHouseObject(house)
            
            return cell
            
        } else {
            
            let cell:HouseListViewCell = tableView.dequeueReusableCell(withIdentifier: "mainCell_simple") as! HouseListViewCell
            
            cell.setHouseObject(house)
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.presenter.houses.count - 3 <= (indexPath as NSIndexPath).row {
            
            presenter.nextPage()
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view:EDAllHousesListTitleView = Bundle.main.loadNibNamed("EDAllHousesListTitleView", owner: self, options: nil)![0] as! EDAllHousesListTitleView
    
        view.titleTextField.text = presenter.currentCity.name
        
        view.titleTextField.inputView = cityPicker

        view.presenter = presenter
        
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    @IBAction func searchAction(_ sender: AnyObject) {

        present(searchController, animated: true, completion: nil)
        //showDetailViewController(searchController, sender: nil)
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
       // presenter.updateHouses(searchController.searchBar.text!)
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        //presenter.updateHouses(searchController.searchBar.text!)
        
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        
        presenter.updateHouses("")
    }
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return presenter.townsList.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let city:EDCity = presenter.townsList[component] as! EDCity
        
        return city.name
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        presenter.currentCity = presenter.townsList[component] as! EDCity
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        presenter.updateHouses(searchBar.text!)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
