//
//  EDApiManager.swift
//  Weather
//
//  Created by Паша on 20.05.16.
//  Copyright © 2016 Паша. All rights reserved.
//

import UIKit
import Alamofire

let baseURL:String = "http://api.devmoydom.ru/v1/" as String
public var apiToken: NSString! = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjYzOTksImlhdCI6MTQ3NDMxOTIzMywiZXhwIjoxNDc0OTI0MDMzfQ.UAA3lrQ8VC8w4_iL1dooqYNE848rb38NNPvIxcqMQyE"

var requset: Request!

//Функции для регистрации в приложении
public func phoneAuth(_ AuthPass: String, AuthLogin: String, success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping (_ error: NSError)->Void) -> Request {
    
    return Alamofire.request(baseURL + "auth/login", method: .post, parameters: ["password": AuthPass, "login": AuthLogin])
        .responseJSON { response in

            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }

}

public func  socialNetworkAuth(_ AuthPass: String, AuthLogin: String, success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping (_ error: NSError)->Void) -> Request {
    
    return Alamofire.request(baseURL+"auth/login", method: .post,  parameters: ["password": AuthPass, "login": AuthLogin])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }

}

//Функции для регистрации в приложении
public func PhoneRegistration(_ RegName: String, RegSex: Int, RegPhone: String, RegPass: String, success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping ((_ error: NSError)->Void)) -> Request {
    
    return Alamofire.request(baseURL + "auth/signup", method: .post , parameters: ["name": RegName, "sex":RegSex, "value":RegPhone, "password":RegPass])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }
}

public func ConfirmRegistration(_ ConfirmCode: Double, success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping ((_ error: NSError)->Void)) -> Request {


    return Alamofire.request(baseURL + "auth/confirm", method: .post, parameters: ["code": ConfirmCode], headers: ["Authorization": apiToken as String]).responseJSON { response in
        
        if let JSON = response.result.value {
            
            success(JSON as! NSDictionary)
            
        } else {
            
            failure(response.result.error! as NSError)
        }
    }
}

public func ResendSMS(_ RegPhone: String, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {

        return Alamofire.request(baseURL + "auth/resend", method: .post, parameters: ["phone":RegPhone], headers: ["Authorization": apiToken as String])
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    
                    success(JSON as! NSDictionary)
                    
                } else {
                    
                    failure(response.result.error! as NSError)
                }
        }
}

//Функции для регистрации дома в приложении

public func LoadCityList(_ AppToken: String, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {

    return Alamofire.request(baseURL + "location/cities", method: .get, headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }
}

public func LoadStreetList(_ AppToken: String, IDOfCity: String, StreetName: String, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {

    let GetStreetURL:String = String(baseURL + "location/streets?city="+IDOfCity+"&street="+StreetName)
//    let SafeGetStreetUrl = GetStreetURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
    let urlwithPercentEscapes = GetStreetURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
    
    return Alamofire.request(urlwithPercentEscapes!, method: .get, headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }
    
}

public func SearchHouses(_ AppToken: String, IDOfCity: String, StreetName: String, page: Int, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {

    let GetHouseURL = String(baseURL + "houses/search?city="+IDOfCity+"&q="+StreetName+"&page="+String(page))
    let urlwithPercentEscapes = GetHouseURL?.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
    
    return Alamofire.request(urlwithPercentEscapes!, method: .get,  headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }

}

public func LoadHouseList(_ AppToken: String, StreetId: Int, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {
    
    let GetHouseURL = String(baseURL + "location/houses?street="+String(StreetId))
    //let SafeGetHouseUrl = GetHouseURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
    let urlwithPercentEscapes = GetHouseURL?.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
    
    
    return Alamofire.request(urlwithPercentEscapes!, method: .get,  headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }

    
}

public func HouseRegistraton(_ AppToken: String, HouseID: NSNumber, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {
    
    return Alamofire.request(baseURL + "users/objects", method: .post, parameters: ["houseId": HouseID], headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }
}

public func SendRequestForAdditionHome(_ AppToken: String, address: String, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {
    
    return Alamofire.request(baseURL + "location/request", method: .post, parameters: ["address": address], headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }
    
}

public func LoadMyHousesList(_ AppToken: String, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {
    
    return Alamofire.request(baseURL + "users/objects", method: .get , headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }
    
}

public func RemoveHouseFromMyObjects (_ AppToken: String, houseId: Int, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) -> Request {
    
    return Alamofire.request(baseURL + "users/objects/" + String(houseId), method: .delete , headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }
    
}

public func GetInfoForObject(_ AppToken: String, houseId: Int, from: Float, to: Float, count: Int, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) ->Request {
    
    var url = "users/objects/" + String(houseId)
    
    if from != 0 {
        
        url = url + "&from=" + String(from)
        
    }
    
    if to != 0 {
        
        url = url + "&to=" + String(to)
        
    }
    
    if count != 0 {
        
        url = url + "&count=" + String(count)
        
    }
    
    return Alamofire.request(baseURL + url, method: .get , headers: ["Authorization": AppToken])
        .responseJSON { response in
            
            if let JSON = response.result.value {
                
                success(JSON as! NSDictionary)
                
            } else {
                
                failure(response.result.error! as NSError)
            }
    }

    
}
