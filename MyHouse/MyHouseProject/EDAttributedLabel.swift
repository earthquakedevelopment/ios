//
//  EDAttributedLabel.swift
//  MyHouseProject
//
//  Created by Павел on 31.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

@IBDesignable class EDAttributedLabel: UILabel {

    @IBInspectable var fontSize: CGFloat = 18.0
    
    @IBInspectable var fontFamily: String = "HelveticaNeueCyr"
    
    @IBInspectable var fontSpacing: CGFloat = 5.0
    
    
    override func awakeFromNib() {
        let attrString = NSMutableAttributedString(attributedString: self.attributedText!)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: self.fontFamily, size: self.fontSize)!, range: NSMakeRange(0, attrString.length))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = self.fontSpacing
        paragraphStyle.alignment = self.textAlignment
        
        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attrString.length))

        self.attributedText = attrString
    }
    
    internal func setTitle(_ text: String!) {

        let attrString = NSMutableAttributedString(string: text)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: self.fontFamily, size: self.fontSize)!, range: NSMakeRange(0, attrString.length))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = self.fontSpacing
        paragraphStyle.alignment = self.textAlignment
        
        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        
        self.attributedText = attrString
        
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
