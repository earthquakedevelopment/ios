//
//  EDAuthPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 22.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDAuthPresenter: NSObject {
    
    weak var view: AuthViewController?;
    
    init(view: AuthViewController!) {
        
        self.view = view
        
        super.init()
        
    }

    internal func authUserByPhone(_ login: String!, password: String!) {
        SwiftSpinner.show("")
        if login.characters.count == 0  {
            SwiftSpinner.hide()
            let alert = UIAlertController(title: "Ошибка", message: "Поле Логин не может быть пустым", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.view!.present(alert, animated: true, completion: nil);
            
            return
            
        } else if password.characters.count == 0  {
            SwiftSpinner.hide()
            let alert = UIAlertController(title: "Ошибка", message: "Поле Пароль не может быть пустым", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.view!.present(alert, animated: true, completion: nil)
            
            return
        }
        
        EDRegistrationManager.sharedInstance.authUser(login, password: password, success: { (json) in
                SwiftSpinner.hide()
                self.view!.performSegue(withIdentifier: "success", sender: self)
            
            }) { (error) in
                SwiftSpinner.hide()
                let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))

                self.view!.present(alert, animated: true, completion: nil);

        }
        
    }
    
}
