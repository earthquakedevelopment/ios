//
//  EDCity.swift
//  MyHouseProject
//
//  Created by Павел on 02.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDCity: NSObject {
    
    var id: Int = 0
    var name = " - "
    var socr = " - "
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        id = (object["id"]! as! NSNumber).intValue
        
        name = object["name"] as! String
        
        socr = object["socr"] as! String
    }

    internal func toDictionary () -> NSDictionary {
        
        return NSDictionary(dictionary: ["id": id, "name": name, "socr": socr])
        
    }
}
