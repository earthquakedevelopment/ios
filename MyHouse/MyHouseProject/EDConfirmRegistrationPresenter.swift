//
//  EDConfirmRegistrationPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 28.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDConfirmRegistrationPresenter: NSObject {
    
    weak var view: ConfirmRegistrationViewController?;
    
    init(view: ConfirmRegistrationViewController!) {
        
        self.view = view
        
        super.init()
        
    }
    
    internal func checkRegistrationCode(_ code: String) {

            if code.characters.count > 0{
                
                SwiftSpinner.show("")
                
                EDRegistrationManager.sharedInstance.confirmRegistration(code, success: { (json) in
                    
//                    SwiftSpinner.hide()
//                    
//                    self.view!.performSegue(withIdentifier: "SuccessConfirmSegue", sender: self.view!)
                    self.auth()
//                    
                    }, failure: { (error) in

                        if (error.userInfo["message"] is String) {
                            SwiftSpinner.hide()
                            let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.view!.present(alert, animated: true, completion: nil);
                            
                        } else {
                            SwiftSpinner.hide()
                            let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.view!.present(alert, animated: true, completion: nil);
                            
                        }

                        
                })

            } else {
                
                let alert = UIAlertController(title: "Ошибка", message: "Поле с кодом подтверждения не может быть пустым", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default, handler: nil))
                self.view!.present(alert, animated: true, completion: nil);
        }
        
    }
    
    internal func auth() {
        
        EDRegistrationManager.sharedInstance.authUser(EDRegistrationManager.sharedInstance.phone, password: EDRegistrationManager.sharedInstance.pass, success: { (json) in
            SwiftSpinner.hide()
            self.view!.performSegue(withIdentifier: "SuccessConfirmSegue", sender: self)
            
        }) { (error) in
            SwiftSpinner.hide()
            let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.view!.present(alert, animated: true, completion: nil);
            
        }
        
    }

}
