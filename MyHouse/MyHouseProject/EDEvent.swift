//
//  EDEvent.swift
//  MyHouseProject
//
//  Created by Павел on 21.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDContractor: NSObject {
    
    var title:String = ""
    var image:String = ""
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        if object["title"] is String {
            title = object["title"] as! String
        }
        
        if object["image"] is String {
            image = object["image"] as! String
        }
        
    }
}

class EDPeriod: NSObject {
    
    var start: Float = 0
    var end: Float = 0
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        if object["start"] is NSNumber {
            start = (object["start"] as! NSNumber).floatValue
        }
        
        if object["end"] is NSNumber {
            end = (object["end"] as! NSNumber).floatValue
        }
        
    }
    
}

class EDEventStatus: NSObject {
    
    var type: String = ""
    var title: String = ""
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        if object["type"] is String {
            type = object["type"] as! String
        }
        
        if object["title"] is String {
            title = object["title"] as! String
        }
        
    }
    
}

//{
//    "id": 241,
//    "lastModified": 1470035072,
//    "createdAt": 1469777961,
//    "icon": "riser",
//    "title": "Ремонтные работы на ВНС",
//    "contractor": {
//        "title": "КФК Водоканал",
//        "image": null
//    },
//    "period": [
//    "start": 1469739600,
//    "end": 1469780400
//    ],
//    "status": {
//        "type": "ended",
//        "title": "выполнено"
//    },
//    "updates": [
//    {
//    "createdAt": 1470640283
//    }
//    ],
//    "targets": [
//    {
//    "name": "Давыдовский-2"
//    }
//    ],
//    "affectedHouses": [
//    "ул Мира д. 4",
//    "ул Дорожная 2-я д. 26"
//    ]
//}

class EDEvent: NSObject {
    
    var id: Int = 0
    var lastModified: Float = 0
    var createdAt: Float = 0
    var icon: String = ""
    var title: String = ""
    var contractor: EDContractor!
    var period: EDPeriod!
    var status: EDEventStatus!
    let updates: NSMutableArray = NSMutableArray()
    let targets: NSMutableArray = NSMutableArray()
    let affectedHouses: NSMutableArray = NSMutableArray()
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        id = ((object["id"] as! NSNumber).intValue)
        
        if object["lastModified"] is NSNumber {
            lastModified = (object["lastModified"] as! NSNumber).floatValue
        }
        
        if object["createdAt"] is NSNumber {
            createdAt = (object["createdAt"] as! NSNumber).floatValue
        }
        
        if object["icon"] is String {
            icon = object["icon"] as! String
        }
        
        if object["title"] is String {
            title = object["title"] as! String
        }
        
        if object["contractor"] is NSDictionary {
            contractor = EDContractor.init(object: object["contractor"] as! NSDictionary)
        }
        
        if object["period"] is NSDictionary {
            period = EDPeriod.init(object: object["period"] as! NSDictionary)
        }
        
        if object["status"] is NSDictionary {
            status = EDEventStatus.init(object: object["status"] as! NSDictionary)
        }
        
        if object["updates"] is NSArray {
            
            for update in object["updates"] as! NSArray {
                
                let upd = update as! NSDictionary
                
                updates.add(upd["createdAt"])
                
            }
            
        }
        
        if object["targets"] is NSArray {
            
            for update in object["targets"] as! NSArray {
                
                let upd = update as! NSDictionary
                
                targets.add(upd["name"])
                
            }
            
        }
        
        if object["affectedHouses"] is NSArray {
            
            for update in object["affectedHouses"] as! NSArray {
                
                affectedHouses.add(update)
                
            }
            
        }
  
    }
}
