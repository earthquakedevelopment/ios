//
//  EDHouse.swift
//  MyHouseProject
//
//  Created by Павел on 02.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import CoreLocation
//"id": 2067897,
//"address": "пр-кт Мира д. 53А",
//"name": "53А",
//"coordinates": {
//    "lat": "57.775473",
//    "lng": "40.939452"
//}

class EDHouse: NSObject {

    var id: Int = 0
    var coordinates:CLLocation!
    var name = " - "
    var address = " - "
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        id = ((object["id"] as! NSNumber).intValue)
        
        let coord:NSDictionary = object["coordinates"] as! NSDictionary

        coordinates = CLLocation(latitude: ((coord["lat"])! as AnyObject).doubleValue, longitude: ((coord["lng"])! as AnyObject).doubleValue)
        
        name = object["name"] as! String
        
        address = object["address"] as! String
        
    }
}
