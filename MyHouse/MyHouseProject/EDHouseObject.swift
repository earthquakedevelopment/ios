//
//  EDHouseObject.swift
//  MyHouseProject
//
//  Created by Павел on 12.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import Foundation
//{
//    "id": 7582688,
//    "isMy": false,
//    "avatar": "http://static.devmoydom.ru/1461682821571f82853920d.png",
//    "address": "ул. Мира, д. 1",
//    "awards": [
//    "http://static.devmoydom.ru/st1/zgljbgn1/147306054757cd1ec353e55.png"
//    ]
//}

//{
//    "id": 614,
//    "avatar": "http://static.devmoydom.ru/st1/zgljamv5/1469782583579b1a37208e7.png",
//    "name": null,
//    "address": "пр-кт Мира, д. 33",
//    "city": "г. Кострома",
//    "countNotices": 0
//},

class EDHouseObject: NSObject {

    var id: Int = 0
    var isMy: Bool = true
    var avatar = ""
    var name = ""
    var address = ""
    var city = ""
    var countNotices: Int = 0
    var awards:NSArray = []
    var events:NSArray!
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        id = ((object["id"] as! NSNumber).intValue)
        
        if object["isMy"] is NSNumber {
            isMy = (object["isMy"] as! NSNumber).boolValue
        }
        
        if object["avatar"] is String {
            avatar = object["avatar"] as! String
        }
        
        if object["name"] is String {
            name = object["name"] as! String
        }
        
        if object["address"] is String {
            address = object["address"] as! String
        }
        
        if object["city"] is String {
            city = object["city"] as! String
        }
        
        if object["countNotices"] is NSNumber {
            countNotices = (object["countNotices"] as! NSNumber).intValue
        }
        
        if object["awards"] is NSArray {
            awards = object["awards"] as! NSArray
        }
        
    }
}
