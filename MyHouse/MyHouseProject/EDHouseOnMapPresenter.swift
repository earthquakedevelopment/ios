//
//  EDHouseOnMapPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 20.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDHouseOnMapPresenter: NSObject {

    weak var view: EDHouseOnMapViewController?
    
    init(view: EDHouseOnMapViewController!) {
        
        self.view = view
        
        super.init()
        
    }
    
    internal func removeHouseFromMyObjects () {
    
        SwiftSpinner.show("")
        
        EDHousesManager.sharedInstance.deleteHouse((view?.address.house)!, success: { (responseObject) in
            
            SwiftSpinner.hide()
            
            self.view?.navigationController!.popViewController(animated: true)
            
            }) { (error) in
                
                SwiftSpinner.hide()
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }

        }
    
    }
}
