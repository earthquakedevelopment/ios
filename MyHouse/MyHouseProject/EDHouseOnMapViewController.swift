//
//  EDHouseOnMapViewController.swift
//  MyHouseProject
//
//  Created by Павел on 20.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}

class EDHouseOnMapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var addressLabel: UILabel!
    var address:EDAddress! = nil
    @IBOutlet weak var mapView: MKMapView!
    let locationManager:CLLocationManager = CLLocationManager()
    var presenter:EDHouseOnMapPresenter! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = EDHouseOnMapPresenter.init(view: self)
        
        locationManager.requestWhenInUseAuthorization()
        
        addressLabel.text = address.getFullAddress()

        let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        
        let region:MKCoordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2DMake(address.house.coordinates.coordinate.latitude, address.house.coordinates.coordinate.longitude), span: span)
        
        mapView.setRegion(region, animated: true)
        
        let info2 = CustomPointAnnotation()
        info2.coordinate = CLLocationCoordinate2DMake(address.house.coordinates.coordinate.latitude, address.house.coordinates.coordinate.longitude)
        info2.imageName = "icon_map_pin_green"
        
        
        mapView.addAnnotation(info2)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneAction(_ sender: AnyObject) {
        
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func backAction(_ sender: AnyObject) {
        
        presenter.removeHouseFromMyObjects()
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! CustomPointAnnotation
        anView?.image = UIImage(named:cpa.imageName)
        anView?.centerOffset = CGPoint(x: 0, y: -((anView?.image?.size.height)! / 2))
        //CGPointMake(0, -((anView?.image?.size.height)! / 2));
        
        return anView
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
