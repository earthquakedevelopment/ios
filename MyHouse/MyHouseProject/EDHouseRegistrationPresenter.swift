//
//  EDHouseRegistrationPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 02.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDHouseRegistrationPresenter: NSObject {

    
    weak var view: HouseRegistrationViewController?
    weak var search: EDSearchHouseTableViewController?
    
    let address = EDAddress()
    
    internal var townsList: NSMutableArray = NSMutableArray()
    internal var streetsList: NSMutableArray = NSMutableArray()
    internal var housesList: NSMutableArray = NSMutableArray()
    
    init(view: HouseRegistrationViewController!) {
        
        self.view = view
        
        super.init()
        
//        townsList.addObject(EDCity())
//        streetsList.addObject(EDStreet())
//        housesList.addObject(EDHouse())
        
        loadTownsList()
        
    }
    
    func loadTownsList() {
        
        EDHousesManager.sharedInstance.updateCities({ (json) in
            
            self.townsList.removeAllObjects()
            
            for object in json {
                
                let city: NSDictionary = object as! NSDictionary
                
                self.townsList.add(EDCity(object: city))
                
            }

            }) { (error) in
                
                if (error.userInfo["message"] is String) {

                    let alert = UIAlertController(title: "Ошибка!", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) in
                       
                        alert.dismiss(animated: true, completion: nil)
                        
                    }))
                    
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }

                
        }
        
    }
    
    func loadStreetsList() {
        
        if self.address.city == nil {
          SwiftSpinner.hide()
            return
        }
        
        EDHousesManager.sharedInstance.updateStreets(String(address.city.id), street: "", success: { (streets) in

            self.streetsList.removeAllObjects()
            
            for object in streets {
                
                let street: NSDictionary = object as! NSDictionary
                
                self.streetsList.add(EDStreet(object: street))
                
            }
            
           SwiftSpinner.hide()

            }) { (error) in
                
            SwiftSpinner.hide()
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }

            
                
        }
        
    }
    
    func loadStreetsList(_ sString: String) {
        
        
        if self.address.city == nil {
             SwiftSpinner.hide()
            return
        }
        
        EDHousesManager.sharedInstance.updateStreets(String(address.city.id), street: sString, success: { (streets) in
            
            self.streetsList.removeAllObjects()
            
            for object in streets {
                
                let street: NSDictionary = object as! NSDictionary
                
                self.streetsList.add(EDStreet(object: street))
                
            }
            
            self.search?.items = self.streetsList

            self.search?.reloadData()
            
        }) { (error) in

            if (error.userInfo["message"] is String) {
                
                let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.view!.present(alert, animated: true, completion: nil);
                
            } else {
                
                let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.view!.present(alert, animated: true, completion: nil);
                
            }
            
            
            
        }
    }

    func loadHousesList(_ page: Int) {
        
        if self.address.street == nil {
             SwiftSpinner.hide()
            return
        }
        
        if page == 1 {
            
            self.housesList.removeAllObjects()
            
        }
        
        EDHousesManager.sharedInstance.updateHouses( address.street.id ,success: { (streets) in
            
            for object in streets {
                
                let house: NSDictionary = object as! NSDictionary
                
                self.housesList.add(EDHouse(object: house))
                
            }

             SwiftSpinner.hide()

            
            }) { (error) in
                
                 SwiftSpinner.hide()
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }

        }
    }
    
    func loadHousesList(_ page: Int, sString: String) {
        
        if self.address.street == nil {
             SwiftSpinner.hide()
            return
        }
        
        
        EDHousesManager.sharedInstance.updateHouses( address.street.id ,success: { (streets) in
            
            self.housesList.removeAllObjects()
            
            for object in streets {
                
                let house: NSDictionary = object as! NSDictionary
                
                self.housesList.add(EDHouse(object: house))
                
            }

            self.search?.items = self.housesList
            
            self.search?.reloadData()
            
             SwiftSpinner.hide()
 
            
        }) { (error) in
            
             SwiftSpinner.hide()
            
            if (error.userInfo["message"] is String) {
                
                let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.view!.present(alert, animated: true, completion: nil);
                
            } else {
                
                let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.view!.present(alert, animated: true, completion: nil);
                
            }
            
        }
    }
    
    func addHouse() {
        
        if self.address.isCorrect() {
            
            EDHousesManager.sharedInstance.addAddress(address, success: { (responseObject) in
                
                UserDefaults.standard.set(self.address.city.toDictionary(), forKey: "currentCity")
                
                self.view?.performSegue(withIdentifier: "MyObjects", sender: nil)
                
            }) { (error) in
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }
                
                
            }
            
        } else {

            self.view?.performSegue(withIdentifier: "sendRequestForAddingAddress", sender: nil)
            
        }

    }
}
