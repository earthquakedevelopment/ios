//
//  EDHousesInfo.swift
//  MyHouseProject
//
//  Created by Павел on 21.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

//{
//    "status": "ok",
//    "card": {
//        "id": 614,
//        "avatar": "http://static.devmoydom.ru/st1/zgljamv5/1469782583579b1a37208e7.png",
//        "name": null,
//        "address": "пр-кт Мира, д. 33",
//        "city": "г. Кострома",
//        "countNotices": 1
//    },
//    "incidents": [
//    {
//    "id": 241,
//    "lastModified": 1470035072,
//    "createdAt": 1469777961,
//    "icon": "riser",
//    "title": "Ремонтные работы на ВНС",
//    "contractor": {
//    "title": "КФК Водоканал",
//    "image": null
//    },
//    "period": [
//    "start": 1469739600,
//    "end": 1469780400
//    ],
//    "status": {
//    "type": "ended",
//    "title": "выполнено"
//    },
//    "updates": [
//    {
//    "createdAt": 1470640283
//    }
//    ],
//    "targets": [
//    {
//    "name": "Давыдовский-2"
//    }
//    ],
//    "affectedHouses": [
//    "ул Мира д. 4",
//    "ул Дорожная 2-я д. 26"
//    ]
//    }
//    ]
//}

import UIKit

class EDHousesInfo: NSObject {
    
    var card:EDHouseObject!
    
    internal override init() {
        super.init()
    }
    
    internal init(object: NSDictionary) {
        
        super.init()
        
        card = EDHouseObject(object: object["card"] as! NSDictionary)

        
    }


}
