//
//  EDHousesManager.swift
//  MyHouseProject
//
//  Created by Павел on 02.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import Alamofire
class EDHousesManager: NSObject {

    static let sharedInstance = EDHousesManager()
    
    var request:Request!

    fileprivate override init() {
    }
    
    internal func updateCities(_ success: @escaping ((_ cities: NSArray) -> Void), failure: @escaping ((_ error: NSError)->Void)) {
        
         LoadCityList(apiToken as String, success: { (json) in
            
            if (json["status"]! as! String == "ok") {

                    success((json["cities"] as! NSArray))

            } else {

                    failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))

            }

            }) { (error) in

                failure(error)
        }
        
    }
    
    internal func updateStreets(_ cityId: String, street: String, success: @escaping (_ streets: NSArray) -> Void, failure: @escaping (_ error: NSError)->Void) {
        
        LoadStreetList(apiToken as String, IDOfCity: cityId, StreetName: street, success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                success((json["streets"] as! NSArray))
                
            } else {
                
                failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
                
            }
            
            }) { (error) in
                
                failure(error)
                
        }
        
    }
    
    internal func updateHouses(_ streetId: Int, success: @escaping (_ streets: NSArray) -> Void, failure: @escaping (_ error: NSError)->Void) {

        requset = LoadHouseList(apiToken as String, StreetId: streetId, success: { (json) in
        
            if (json["status"]! as! String == "ok") {

                    success((json["houses"] as! NSArray))

            } else {

                    failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))

            }
        
        }) { (error) in
            
            failure(error)
            
        }
    }
    
    internal func searchHouses(_ cityId: String, searchString: String, page: Int, success: @escaping ((_ houses: NSArray, _ lastPage: Bool) -> Void), failure: @escaping ((_ error: NSError)->Void)) {
        
        request = SearchHouses(apiToken as String, IDOfCity: cityId, StreetName: searchString, page: page, success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                success((json["houses"] as! NSArray), json["nextPage"] is NSNull)
                
            } else {
                
                failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
                
            }
            
            }, failure: { (error) in
                
                failure(error)
        })
        
    }

    internal func addAddress(_ address: EDAddress, success: @escaping (_ responseObject: NSDictionary) -> Void, failure: @escaping (_ error: NSError)->Void) {
        
        HouseRegistraton(apiToken as String, HouseID: NSNumber(value: address.house.id), success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                success(json)
                
            } else {
                
                failure(NSError(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
                
            }

            
            }) { (error) in
                
                failure(error)
                
        }
        
    }
    
    internal func sendRequestForAddingHome(_ AppToken: String, address: String, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) {
        
        SendRequestForAdditionHome(apiToken as String, address: address, success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                success(json)
                
            } else {
                
                failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
                
            }
            
            }) { (error) in
                
                failure(error)
                
        }
        
    }
    
    internal func loadHousesList(_ success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) {
        
        if request != nil {
            request.resume()
        }
        
        
        request = LoadMyHousesList(apiToken as String, success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                success(json)
                
            } else {
                
                failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
                
            }
            
            }) { (error) in
                
                failure(error)
                
        }
        
    }
    
    internal func deleteHouse(_ house: EDHouse, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) {
        
        request = RemoveHouseFromMyObjects(apiToken as String, houseId: house.id, success: { (json: NSDictionary) in
            
            if (json["status"]! as! String == "ok") {
                
                success(json)
                
            } else {
                
                failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
                
            }

            
            }, failure: { (error) in
                
                failure(error)
                
        })
        
    }
    
    internal func getInfoForObject (_ house: EDHouseObject, from: Float, to: Float, count: Int, success: @escaping ((_ json: NSDictionary) -> Void), failure: @escaping ((_ error: NSError)->Void)) {
        
        request = GetInfoForObject(apiToken as String, houseId: house.id, from: from, to: to, count: count, success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                success(json)
                
            } else {
                
                failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
                
            }

            
            }, failure: { (error) in
                
                failure(error)
                
        })
        
    }
}
