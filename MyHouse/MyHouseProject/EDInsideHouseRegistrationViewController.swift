//
//  EDInsideHouseRegistrationViewController.swift
//  MyHouseProject
//
//  Created by Павел on 19.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftValidator
import SwiftSpinner

class EDInsideHouseRegistrationViewController: HouseRegistrationViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        validator.registerField(HouseTextField, errorLabel: errorHouseLabel, rules: [RequiredRule()])
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        definesPresentationContext = true

    }
    
    override func addLaterAction(_ sender: AnyObject) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        if textField.tag == 2 {
            
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let resultSearchVC:EDSearchHouseTableViewController = storyboard.instantiateViewController(withIdentifier: "searchHouse") as! EDSearchHouseTableViewController
            resultSearchVC.presenter = self.presenter
            resultSearchVC.presenter.search = resultSearchVC
            resultSearchVC.type = .street
            
            let searchVC:UISearchController = UISearchController(searchResultsController: resultSearchVC)
            searchVC.searchResultsUpdater = resultSearchVC
            searchVC.hidesNavigationBarDuringPresentation = true
            searchVC.dimsBackgroundDuringPresentation = true
            searchVC.searchBar.sizeToFit()
            searchVC.delegate = resultSearchVC
    
            present(searchVC, animated: true, completion: nil)
            
            
        }
        if textField.tag == 3 {
            
            if self.presenter.housesList.count > 0 {
                
                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                
                let resultSearchVC:EDSearchHouseTableViewController = storyboard.instantiateViewController(withIdentifier: "searchHouse") as! EDSearchHouseTableViewController
                resultSearchVC.presenter = self.presenter
                resultSearchVC.presenter.search = resultSearchVC
                resultSearchVC.type = .house
                
                let searchVC:UISearchController = UISearchController(searchResultsController: resultSearchVC)
                searchVC.searchResultsUpdater = resultSearchVC
                searchVC.hidesNavigationBarDuringPresentation = true
                searchVC.dimsBackgroundDuringPresentation = true
                searchVC.searchBar.sizeToFit()
                searchVC.searchBar.keyboardType = .numbersAndPunctuation
                //searchVC.searchBar.text = presenter.address.street.name + " "
                searchVC.delegate = resultSearchVC
                
                present(searchVC, animated: true, completion: nil)
                
            }
            
        }
        
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MyObjects" {
            
            let dest:EDHouseOnMapViewController = segue.destination as! EDHouseOnMapViewController
            
            dest.address = presenter.address
            
        }
        
    }

}
