//
//  EDMenuPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 23.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDMenuPresenter: NSObject {

    weak var view: EDMenuViewController!;
    
    
    init(view: EDMenuViewController!) {
        
        self.view = view
        
        super.init()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "kEDSocialNetworkLoginSuccess"), object: nil, queue: OperationQueue()) { (notification) in
            
            OperationQueue.main.addOperation({ 
                self.view.performSegue(withIdentifier: "success", sender: self)
            })
            
            
            
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "kEDSocialNetworkLoginFailure"), object: nil, queue: OperationQueue()) { (notification) in
            
            OperationQueue.main.addOperation({
                
                let alert = UIAlertController(title: "Ошибка", message: "Авторизация не удалась. Попробуйте позже", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                
                self.view!.present(alert, animated: true, completion: nil);
                
            })
        }
        
    }
    
    internal func authWithSocialNetwork(_ social: Int) {
        
        EDRegistrationManager.sharedInstance.authWithSocialNetwork(social, view: (self.view.view)!)
        
        
    }
}
