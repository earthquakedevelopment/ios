//
//  EDMenuViewController.swift
//  MyHouseProject
//
//  Created by Павел on 22.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner


class EDMenuViewController: UIViewController {

    var webView:UIWebView!
    
    var presenter:EDMenuPresenter! = nil
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuBackgroundView: UIView!
    @IBOutlet weak var registrationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuBackgroundView.layer.cornerRadius = 3.0
        registrationButton.layer.cornerRadius = 3.0
        
        presenter = EDMenuPresenter.init(view: self)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func authAction(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "auth", sender: nil)
        
    }
    @IBAction func registrationAction(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "registration", sender: nil)
        
    }
    @IBAction func socialAuthAction(_ sender: UIButton) {
        
        presenter.authWithSocialNetwork(sender.tag)

    }
    

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
