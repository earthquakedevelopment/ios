//
//  EDMyObjectsPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 12.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDMyObjectsPresenter: NSObject {

    weak var view: EDMyObjectsViewController?;
    
    var houses: NSMutableArray = NSMutableArray()
    
    var isLimitReached:Bool = false
    
    var totalNotices:Int = 0
    
    init(view: EDMyObjectsViewController!) {
        
        self.view = view
        
        super.init()
        
    }
    
    internal func loadHouses () {
        
        SwiftSpinner.show("")
        
        EDHousesManager.sharedInstance.loadHousesList({ (json) in
            
            SwiftSpinner.hide()
            
            self.houses.removeAllObjects()
            
            let responseObject:NSDictionary = json
            
            self.isLimitReached = ((responseObject.object(forKey: "isLimitReached") as? NSNumber )!.boolValue)
            
            self.totalNotices = Int((responseObject.object(forKey: "totalNotices")! as! NSString).intValue)
            
            if responseObject.object(forKey: "houses") is NSArray {
                let houses:NSArray = responseObject.object(forKey: "houses") as! NSArray
                
                for object in houses {
                    
                    let house: NSDictionary = object as! NSDictionary
                    
                    self.houses.add(EDHouseObject(object: house))
                    
                }
                
            }

            if self.houses.count > 0 {
                self.view?.tableView.isHidden = false
            } else {
                self.view?.tableView.isHidden = true
            }
            
            
            self.view?.tableView.reloadData()
            
            }) { (error) in
                
                SwiftSpinner.hide()
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }

                
        }
        
    }
    
    internal func getEventsForHouseAtIndexPath(indexPath: IndexPath) {
        
        SwiftSpinner.show("")

        let house:EDHouseObject = houses[indexPath.row] as! EDHouseObject
        
        EDHousesManager.sharedInstance.getInfoForObject(house, from: 0, to: 0, count: 0, success: { (result) in
            
            if !(result["incidents"] is NSNull) {
                
                let events:NSArray = result["incidents"] as! NSArray
                
                let resultObjects:NSMutableArray = NSMutableArray()
                
                for event in events {
                    
                    resultObjects.add(EDEvent.init(object: event as! NSDictionary))
                    
                }
                
                house.events = resultObjects
                
                self.view?.performSegue(withIdentifier: "events", sender: house)
                
            }
            
            
            }) { (error) in
                
        }
    }
    
}
