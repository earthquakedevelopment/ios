//
//  EDMyObjectsViewController.swift
//  MyHouseProject
//
//  Created by Павел on 30.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDMyObjectsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var presenter:EDMyObjectsPresenter! = nil
    
    @IBOutlet weak var addItemButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = EDMyObjectsPresenter.init(view: self)
        
        
        
        addItemButton.layer.cornerRadius = 4.0
        tableView.isHidden = true
        
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        presenter.loadHouses()
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
    }

    @IBAction func SideMenuButton(_ sender: AnyObject) {
        self.slideMenuController()?.openRight()
    }

    @IBAction func seeAllHouses(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "seeAllHouses", sender: nil)
    }
    
    @IBAction func addHouseAction(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "addHouse", sender: nil)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.houses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let house:EDHouseObject = presenter.houses[(indexPath as NSIndexPath).row] as! EDHouseObject
        
        if house.name.characters.count > 0 {
            
            let cell:HouseListViewCell = tableView.dequeueReusableCell(withIdentifier: "mainCell") as! HouseListViewCell
            
            cell.setHouseObject(house)
            
            return cell
            
        } else {
            
            let cell:HouseListViewCell = tableView.dequeueReusableCell(withIdentifier: "mainCell_simple") as! HouseListViewCell
            
            cell.setHouseObject(house)
            
            return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        presenter.getEventsForHouseAtIndexPath(indexPath: indexPath)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if presenter.isLimitReached {
            
            let view:MaxHouseFooterViewCell = (Bundle.main.loadNibNamed("MaxHouseFooterViewCell", owner: self, options: nil)![0] as? MaxHouseFooterViewCell)!
            
            return view

        } else {
            
            let view:AddHouseFooterViewCell = (Bundle.main.loadNibNamed("AddHouseFooterViewCell", owner: self, options: nil)![0] as? AddHouseFooterViewCell)!
            
            view.view = self
            
            return view
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if presenter.isLimitReached {
            
            return 480
            
        } else {
            
            return 223
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.01
        
    }

}
