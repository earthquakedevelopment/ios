//
//  EDRegPresenter.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 14.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import Alamofire

public var RegToken = "RegToken"
public var RegStatus = "RegStatus"
public var ConfirmStatus = "ConfirmStatus"
public var RegStatusText = "RegStatusText"
public var login = "Login"
public var pass = "Password"
public var AppToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjYzNDksImlhdCI6MTQ3MTY0MDA1OSwiZXhwIjoxNDcyMjQ0ODU5fQ.VDTYRfTMmlZvJh5xFSXdlAdODH9Sc0XSR0Sr-GhyblY"
public var AuthStatus = "AuthStatus"
public var UserPhone = "Phone"
public var ResendCounter = 0
public var CityNames = ["String"]
public var CityCodes = ["String"]
public var CityRow = 0
public var StreetRow = 0
public var HouseID = 0
public var HouseCardID = "0"
public var CityData = ["ID": "CityName"]
public var StreetData = [["id": "4795591", "name": "Центральная", "socr": "пр-кт", "cid": "657", "City": "Кострома"]]
public var HouseData = [["address": "ул. Мира, д. 1", "avatar": "http://static.realtor.im/1461682821571f82853920d.png","City": "г. Кострома", "id": 7582688]]