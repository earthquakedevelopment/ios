//
//  EDRegistrationManager.swift
//  MyHouseProject
//
//  Created by Павел on 22.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftyJSON


class EDRegistrationManager: NSObject, UIWebViewDelegate {
    
    internal var token: NSString!

    var registrationView:UIView!
    var webView:UIWebView!
    
    var phone:String!
    var pass:String!
    
    // MARK: - property
    
    static let sharedInstance = EDRegistrationManager()
    
    // MARK: - init
    
    fileprivate override init() {
    }

    internal func authUser(_ login: String, password: String, success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping (_ error: NSError)->Void) {
        
        phoneAuth(password, AuthLogin: login, success: { (json) in

            if (json["status"]! as! String == "ok") {
                
                apiToken = ("Bearer " + (json["token"] as! String)) as NSString
                
                print(apiToken)
                
                success(json)
                
            } else {
                
                failure(NSError(domain: baseURL, code: ((json["errorCode"] as! NSNumber).intValue), userInfo: ["message": json["message"]!]))
            }
            
            
            
            
        }) { (error) in
            
            failure(error)

        }
        
    }
    
    internal func authWithSocialNetwork(_ network: Int, view: UIView) {
        
        registrationView = UIView(frame: CGRect(x: 0, y: view.frame.size.height, width: view.frame.size.width, height: view.frame.size.height))
        
        
        let blurBackground:UIVisualEffectView = UIVisualEffectView(frame: view.frame)
        
        let blurEffect:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        
        blurBackground.effect = blurEffect
        
        registrationView.addSubview(blurBackground)
        
        
        
        webView = UIWebView(frame: CGRect(x: 30, y: 30, width: view.frame.size.width-60, height: view.frame.size.height-60))
        
        webView.delegate = self
        
        webView.layer.cornerRadius = 8.0
        
        webView.clipsToBounds = true
        
        registrationView.addSubview(webView)
        
        

        let closeButton:UIButton = UIButton(frame: CGRect(x: view.frame.size.width-40, y: 20, width: 20, height: 20))
        
        closeButton.backgroundColor = UIColor.white
        
        closeButton.setImage(UIImage(named: "icon_close"), for: UIControlState())

        closeButton.layer.cornerRadius = closeButton.frame.size.height/2;
        
        closeButton.clipsToBounds = true;
        
        closeButton.addTarget(self, action: #selector(self.closeWebView), for: UIControlEvents.touchUpInside)
        
        registrationView.addSubview(closeButton)
        
        
        
        view.addSubview(registrationView)

        switch network {
        case 0:
            
            let request = URLRequest(url: URL(string: "http://api.realtor.im/v1/auth/social?service=odnoklassniki")!)
            
            webView.loadRequest(request)
            
            break;
            
        case 1:
            
            let request = URLRequest(url: URL(string: "http://api.realtor.im/v1/auth/social?service=vkontakte")!)
            
            webView.loadRequest(request)
            
            break;
            
        case 2:
            
            let request = URLRequest(url: URL(string: "http://api.realtor.im/v1/auth/social?service=facebook")!)
            
            webView.loadRequest(request)
            
            break;
            
        case 3:
            
            let request = URLRequest(url: URL(string: "http://api.realtor.im/v1/auth/social?service=google_oauth")!)
            
            webView.loadRequest(request)
            
            break;
            
        default:
            break
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.registrationView.frame = view.frame;
            
        });

    }
    
    internal func registrationUser(_ username: String, password: String, phone: String, gender: Int, success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping (_ error: NSError)->Void) {
        
        PhoneRegistration(username, RegSex: gender, RegPhone: phone, RegPass: password, success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                self.phone = phone
                
                self.pass = password
                
                apiToken = ("Bearer " + (json["token"] as! String)) as NSString
                
                success(json)
                
            } else {
                
                failure(NSError(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
            }

            
            }) { (error) in
                
                failure(error)
        }
   
    }
    
    internal func confirmRegistration(_ code: String, success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping (_ error: NSError)->Void) {
        
        ConfirmRegistration(Double(code)!, success: { (json) in
            
            if (json["status"]! as! String == "ok") {

                success(json)
                
            } else {
                
                failure(NSError(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
            }

            
            }) { (error) in
                
                failure(error)
        }
        
    }
    
    internal func resendCode(_ success: @escaping (_ json: NSDictionary) -> Void, failure: @escaping (_ error: NSError)->Void) {
        
        ResendSMS(self.phone, success: { (json) in
            
            if (json["status"]! as! String == "ok") {
                
                success(json)
                
            } else {
                
                failure(NSError.init(domain: baseURL, code: 0, userInfo: ["message": json["message"]!]))
            }
            
            }) { (error) in
                
                failure(error)
                
        }
        
    }
    
    func closeWebView() {
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.registrationView.frame = CGRect(x: 0, y: self.webView.frame.size.height, width: self.webView.frame.size.width, height: self.webView.frame.size.height)
            
            
            }, completion: { (finished) in
                
                self.registrationView.removeFromSuperview()
                
        })
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        let html:String = webView.stringByEvaluatingJavaScript(from: "document.body.innerHTML")!
        
        if html.contains("\"status\": \"ok\"") {
            
            closeWebView()
            
            let socialToken = html.substring(with: (html.range(of: "\"token\": \"")?.upperBound)! ..< (html.range(of: "\"\n}</pre>")?.lowerBound)!)
            
            if socialToken.characters.count == 0 {
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDSocialNetworkLoginFailure"), object: self)
                
            } else {
                
                    token = socialToken as NSString!
                NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDSocialNetworkLoginSuccess"), object: self)
                
            }
            
        } else if html.contains("\"errorCode\":") {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kEDSocialNetworkLoginFailure"), object: self)
            
            closeWebView()
            
        }
        
        print(JSON.parse(html))
        
    }
    
}
