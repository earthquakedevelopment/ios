//
//  EDRegPresenter.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 14.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner


public var RegStatus = "RegStatus"
public var RegStatusText = "RegStatusText"
public var AppToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjYzNDksImlhdCI6MTQ3MTY0MDA1OSwiZXhwIjoxNDcyMjQ0ODU5fQ.VDTYRfTMmlZvJh5xFSXdlAdODH9Sc0XSR0Sr-GhyblY"
public var AuthStatus = "AuthStatus"
public var UserPhone = "+7 (925) 889-58-70"
public var ResendCounter = 0
public var CityNames = ["String"]
public var CityCodes = ["String"]
//public var CityRow = 0
public var StreetRow = 0
public var HouseID = 0
public var HouseCardID = "0"
//public var CityData = ["ID": "CityName"]
public var StreetData = [["id": "4795591", "name": "Центральная", "socr": "пр-кт", "cid": "657", "City": "Кострома"]]
public var HouseData = [["address": "ул. Мира, д. 1", "avatar": "http://static.realtor.im/1461682821571f82853920d.png","City": "г. Кострома", "id": 7582688]]

class EDRegistrationPresenter: NSObject {

    weak var view: RegistrationViewController?;

    init(view: RegistrationViewController!) {
        
        self.view = view
        
        super.init()
        
    }
    
    internal func registrationUser(_ username: String, password: String, phone: String, gender: Int) {
        
        if username.characters.count < 3 {
            let alert = UIAlertController(title: "Ошибка", message: "Введенное Ф.И.О. слишком короткое", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default, handler: nil))
            view!.present(alert, animated: true, completion: nil);
        } else if username.characters.count > 50{
                let alert = UIAlertController(title: "Ошибка", message: "Введенное Ф.И.О. слишком длинное", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default, handler: nil))
                view!.present(alert, animated: true, completion: nil);
            } else if (password.characters.count < 6) {
                    let alert = UIAlertController(title: "Ошибка", message: "Введенный пароль слишком короткий", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    view!.present(alert, animated: true, completion: nil);
                } else if password.characters.count > 50{
                        let alert = UIAlertController(title: "Ошибка", message: "Введенный пароль слишком длинный", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default, handler: nil))
                        view!.present(alert, animated: true, completion: nil);
                    } else {
            
                        UserPhone = phone
            
                        SwiftSpinner.show("")
            
                        EDRegistrationManager.sharedInstance.registrationUser(username, password: password, phone: phone, gender: gender+1, success: { (json) in
                            
                                SwiftSpinner.hide()
                            
                                self.view!.performSegue(withIdentifier: "RegConfirmSegue", sender: self.view!)
                            
                            }, failure: { (error) in
                                
                                if (error.userInfo["message"] is String) {
                                    SwiftSpinner.hide()
                                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.view!.present(alert, animated: true, completion: nil);
                                    
                                } else {
                                    SwiftSpinner.hide()
                                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.view!.present(alert, animated: true, completion: nil);
                                    
                                }
                                
                        })
            
                    }
        
        
    }

}

