//
//  EDRequestAddressPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 11.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner
class EDRequestAddressPresenter: NSObject {
    
    weak var view: EDRequestAddressViewController?
    
    init(view: EDRequestAddressViewController!) {
        
        self.view = view
        
        super.init()
        
    }
    
    func sendRequest() {
        
        if view?.addressTextField.text == nil || view?.addressTextField.text?.characters.count == 0 {
            
            let alert = UIAlertController(title: "Ошибка", message: "Заполните поле адреса", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.view!.present(alert, animated: true, completion: nil);
            
            return
            
        }
        
        EDHousesManager.sharedInstance.sendRequestForAddingHome(apiToken as String, address: (view?.addressTextField.text)!, success: { (json) in
            
            SwiftSpinner.hide()
            
            self.view?.performSegue(withIdentifier: "success", sender: nil)
            
            }) { (error) in
                
                SwiftSpinner.hide()
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }

                
        }
        
    }

}
