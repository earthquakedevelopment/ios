//
//  EDRequestAddressViewController.swift
//  MyHouseProject
//
//  Created by Павел on 11.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDRequestAddressViewController: UIViewController {

    var addressStr:String!
    var address:EDAddress!
    var presenter:EDRequestAddressPresenter! = nil
    
    @IBOutlet weak var whiteBackgroundView: UIView!
    @IBOutlet weak var sendRequestButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = EDRequestAddressPresenter.init(view: self)
        
        sendRequestButton.layer.cornerRadius = 4.0
        sendRequestButton.clipsToBounds = true
        whiteBackgroundView.layer.cornerRadius = 4.0
        whiteBackgroundView.clipsToBounds = true
        
        addressTextField.text = addressStr
        // Do any additional setup after loading the view.
    }

    @IBAction func sendRequestAction(_ sender: AnyObject) {
        
        SwiftSpinner.show("")
        
        presenter.sendRequest()
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
