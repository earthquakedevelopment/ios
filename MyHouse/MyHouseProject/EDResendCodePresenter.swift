//
//  EDResendCodePresenter.swift
//  MyHouseProject
//
//  Created by Павел on 28.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

class EDResendCodePresenter: NSObject {
    
    weak var view: ResendConfirmRegistrationViewController?;
    var SMSTimer = Timer()
    var SMSTimerCount = 300
    var resendCountLeft = 3;
    
    init(view: ResendConfirmRegistrationViewController!) {
        
        self.view = view

        super.init()

    }
    
    internal func resendCodeAction() {
        
        EDRegistrationManager.sharedInstance.resendCode({ (json) in
            
                self.startTimer()
            
                self.view?.timerStarted()
            
            }) { (error) in
                
                if (error.userInfo["message"] is String) {
                    
                    let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                } else {
                    
                    let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.view!.present(alert, animated: true, completion: nil);
                    
                }
                
        }
        
    }
    
    internal func checkRegistrationCode(_ code: String) {
        
        SwiftSpinner.show("")
        
        if code.characters.count > 0{
            
            EDRegistrationManager.sharedInstance.confirmRegistration(code, success: { (json) in
                
                self.auth()
                
                }, failure: { (error) in
                    
                    if (error.userInfo["message"] is String) {
                        SwiftSpinner.hide()
                        let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.view!.present(alert, animated: true, completion: nil);
                        
                    } else {
                        SwiftSpinner.hide()
                        let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.view!.present(alert, animated: true, completion: nil);
                        
                    }
                    
                    
            })
            
        } else {
            SwiftSpinner.hide()
            let alert = UIAlertController(title: "Ошибка", message: "Поле с кодом подтверждения не может быть пустым", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default, handler: nil))
            self.view!.present(alert, animated: true, completion: nil);
        }
        
    }
    
    internal func auth() {
        
        EDRegistrationManager.sharedInstance.authUser(EDRegistrationManager.sharedInstance.phone, password: EDRegistrationManager.sharedInstance.pass, success: { (json) in
            
            SwiftSpinner.hide()
            
            self.view!.performSegue(withIdentifier: "SuccessConfirmSegue", sender: self)
            
        }) { (error) in
            SwiftSpinner.hide()
            let alert = UIAlertController(title: "Ошибка", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.view!.present(alert, animated: true, completion: nil);
            
        }
        
    }

    
    func startTimer() {
        
        if resendCountLeft == 0 {
            
            let alert = UIAlertController(title: "Ошибка", message: "Превышен лимит на запрос смс с кодом подтверждения", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default, handler: nil))
            view!.present(alert, animated: true, completion: nil);
            
        } else {
            
            resetTimer()
            
            SMSTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(changeTime), userInfo: nil, repeats: true)
            
        }
        
    }
    
    func resetTimer() {
        
        SMSTimer.invalidate()
        SMSTimerCount = 300
  
    }
    
    func changeTime() {
        
        if SMSTimerCount > 0 {
            
            SMSTimerCount -= 1
            
            
            var timeLeft:String = "Отправить смс через " + String(SMSTimerCount/60)+":"
            
            if (SMSTimerCount%60>10) {
                
                timeLeft = timeLeft + String(SMSTimerCount%60)
                
            } else {
            
                timeLeft = timeLeft + "0"+String(SMSTimerCount%60)
            }
            
            
            view!.changeLeftTime(timeLeft)
            
        } else {
            
            view!.timerFinished()
            resetTimer()
            resendCountLeft-=1
        }
        
    }

}
