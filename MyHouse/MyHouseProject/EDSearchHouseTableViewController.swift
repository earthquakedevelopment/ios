//
//  EDSearchHouseTableViewController.swift
//  MyHouseProject
//
//  Created by Павел on 06.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftSpinner

public enum EDSearchHouseTableViewControllerType {
    
    case street, house
    
}

class EDSearchHouseTableViewController: UITableViewController, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    
    var type:EDSearchHouseTableViewControllerType = .street
    
    var items:NSMutableArray!

    var filterString: String! {
        
        didSet {

            switch type {
            case .house:
                
                items = self.presenter.housesList.mutableCopy() as! NSMutableArray
                
                if filterString != nil && filterString.characters.count>0 {
                    items.filter(using: NSPredicate(format: "SELF.address contains[c] %@", filterString.components(separatedBy: " ").last!))
                }
                

                tableView.reloadData()
                
            case .street:
                
                self.presenter.loadStreetsList(filterString)

                tableView.reloadData()
                
            }

        }

    }
    
    weak var presenter:EDHouseRegistrationPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch type {
        case .street:
            
            self.presenter.loadStreetsList("")
            
            items = presenter.streetsList
            
        case .house:
            
            self.presenter.loadHousesList(0, sString: presenter.address.street.name)
            
            items = presenter.housesList

        }
        
        tableView.isHidden = false
        
        tableView.reloadData()
    }
    
    internal func reloadData() {
    
        tableView.reloadData()

    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath)

        switch type {
        case .house:
            
            let item:EDHouse = items[(indexPath as NSIndexPath).row] as! EDHouse
            
            cell.textLabel?.text = item.address
            
        case .street:
                
            let item:EDStreet = items[(indexPath as NSIndexPath).row] as! EDStreet
            
            cell.textLabel?.text = item.getFullAddress()
            
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch type {
        case .house:
            
            self.presenter.address.house = items[(indexPath as NSIndexPath).row] as! EDHouse
            self.dismiss(animated: true, completion: nil)
            
        case .street:
            
            self.presenter.address.street = items[(indexPath as NSIndexPath).row] as! EDStreet
            self.dismiss(animated: true, completion: nil)
            
        }
 
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if type == .street {
            return nil
        }
        
        if self.items.count == 0 {
            
            let view:EDSearchNotFound =  Bundle.main.loadNibNamed("EDSearchNotFound", owner: self, options: nil)![0] as! EDSearchNotFound
            
            view.view = self.presenter.view
            
            return view
        }
        
        
        return nil
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if type == .street {
            return 0
        }
        
        if self.items.count == 0 {
            
            return 274
            
        }
        
        
        return 0
        
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if !searchController.isActive {
            
            return
        }
        
        filterString = searchController.searchBar.text

        searchController.searchResultsController?.view.isHidden = false
        
        //self.presenter.view?.HouseTextField.text = filterString
        
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
    }

}
