//
//  EDSearchNotFound.swift
//  MyHouseProject
//
//  Created by Павел on 11.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit



class EDSearchNotFound: UIView {

    weak var view:HouseRegistrationViewController!
    
    @IBOutlet weak var sendRequestButton: UIButton!
    @IBAction func sendRequestAction(_ sender: AnyObject) {
        
        view.performSegue(withIdentifier: "sendRequestForAddingAddress", sender: nil)
        
    }
    
    override func awakeFromNib() {
        
        sendRequestButton.layer.cornerRadius = 4.0
        sendRequestButton.clipsToBounds = true
        
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
