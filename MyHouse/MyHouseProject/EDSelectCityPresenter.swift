//
//  EDSelectCityPresenter.swift
//  MyHouseProject
//
//  Created by Павел on 19.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDSelectCityPresenter: NSObject {

    weak var view: EDSelectCityViewController?
    
    internal var townsList: NSMutableArray = NSMutableArray()
    
    init(view: EDSelectCityViewController!) {
    
        self.view = view
    
        super.init()

        loadTownsList()
    
    }

    func loadTownsList() {
        
        EDHousesManager.sharedInstance.updateCities({ (json) in
            
            self.townsList.removeAllObjects()
            
            for object in json {
                
                let city: NSDictionary = object as! NSDictionary
                
                self.townsList.add(EDCity(object: city))
                
            }
            
            self.view?.tableView.reloadData()
            
        }) { (error) in
            
            if (error.userInfo["message"] is String) {
                
                let alert = UIAlertController(title: "Ошибка!", message: error.userInfo["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) in
                    
                    alert.dismiss(animated: true, completion: nil)
                    
                }))
                
                self.view!.present(alert, animated: true, completion: nil);
                
            } else {
                
                let alert = UIAlertController(title: "Ошибка", message: ((error.userInfo["message"] as? NSDictionary)?.allValues.first as AnyObject).firstObject as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.view!.present(alert, animated: true, completion: nil);
                
            }
            
            
        }
        
    }

    
}
