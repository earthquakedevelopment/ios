//
//  EDSelectCityViewController.swift
//  MyHouseProject
//
//  Created by Павел on 19.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDSelectCityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var presenter:EDSelectCityPresenter!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = EDSelectCityPresenter.init(view: self)
        
        presenter.loadTownsList()
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return presenter.townsList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let city:EDCity = presenter.townsList[indexPath.row] as! EDCity
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "mainCell")!
        
        cell.textLabel?.text = city.name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let city:EDCity = presenter.townsList[indexPath.row] as! EDCity
        
        UserDefaults.standard.set(city.toDictionary(), forKey: "currentCity")
        
        self.performSegue(withIdentifier: "MyObjects", sender: nil)
    }
}
