//
//  EDStreet.swift
//  MyHouseProject
//
//  Created by Павел on 02.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class EDStreet: NSObject {
    
    var cid: Int = 0
    var id: Int = 0
    var city = " - "
    var socr = " - "
    var name = " - "
    
    internal override init() {
        super.init()
    }

    internal init(object: NSDictionary) {
        
        super.init()
        
        id = (object["id"]! as! NSNumber).intValue
        
        cid = (object["cid"]! as! NSNumber).intValue
        
        city = object["city"] as! String
        
        name = object["name"] as! String
        
        socr = object["socr"] as! String
    }
    
    internal func getFullAddress() -> String {
        
        return name + " " + socr
        
    }

}
