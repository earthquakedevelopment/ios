//
//  HouseListViewCell.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 06.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import Kingfisher
import Nuke
import SwiftSVG
import Alamofire


class HouseListViewCell: UITableViewCell, UICollectionViewDataSource {

    var house:EDHouseObject!
    
    @IBOutlet weak var myHomeIconImageView: UIImageView!
    @IBOutlet weak var HouseImage: UIImageView!
    @IBOutlet weak var HouseStatusLabel: UILabel!
    @IBOutlet weak var HouseInfoLabel: EDAttributedLabel!
    @IBOutlet weak var HouseMedalCollection: UICollectionView!
    @IBOutlet weak var notificationsCountLabel: UILabel!
    
    override func awakeFromNib() {
        
        if (notificationsCountLabel != nil) {
            notificationsCountLabel.clipsToBounds = true
            notificationsCountLabel.layer.cornerRadius = 4.0
        }
        
        if (myHomeIconImageView != nil) {
            myHomeIconImageView.clipsToBounds = true
            myHomeIconImageView.layer.cornerRadius = 4.0
        }
        
    }
    
    internal func setHouseObject(_ house: EDHouseObject) {
        
        self.house = house

        Nuke.loadImage(with: URL(string: self.house.avatar)!, into: HouseImage)

        HouseInfoLabel.setTitle(self.house.address)
        
        if (notificationsCountLabel != nil) {
            notificationsCountLabel.text = String(self.house.countNotices)
            
            if self.house.countNotices == 0  {
                notificationsCountLabel.backgroundColor = UIColor(red: 52.0/255.0, green: 168.0/255.0, blue: 83.0/255.0, alpha: 1.0)
            } else {
                notificationsCountLabel.backgroundColor = UIColor(red: 223.0/255.0, green: 54.0/255.0, blue: 42.0/255.0, alpha: 1.0)
            }
        }
        
        if (myHomeIconImageView != nil) {
            
            myHomeIconImageView.isHidden = !house.isMy
            
        }
        
        HouseMedalCollection.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return house.awards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "mainCell", for: indexPath)
        
        let documents:NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let filePath:NSString = documents.appendingPathComponent("\((self.house.awards[indexPath.row] as! String).hash).svg") as NSString
        
        for subview in cell.subviews {
            
            subview.removeFromSuperview()
            
        }
        
        if  NSData(contentsOf: URL(fileURLWithPath: filePath as String)) != nil {
            
            let svgViewFromFile:UIView = UIView(SVGURL: URL(fileURLWithPath: filePath as String))
            
            cell.addSubview(svgViewFromFile)
            
        } else {
        
        Alamofire.request(house.awards[indexPath.row] as! String).responseData { response in
            debugPrint("All Response Info: \(response)")
            
            if let data = response.result.value {

                do {
                    try data.write(to: URL(fileURLWithPath: filePath as String))
                } catch {
                    
                }
                
                print(filePath)
                
                let svgViewFromFile = UIView(SVGURL: URL(fileURLWithPath: filePath as String))

                cell.addSubview(svgViewFromFile)
                
            }

        }
            
        }
        
//        if  NSData(contentsOf: URL(fileURLWithPath: filePath as String)) != nil {
//            
//            let image:UIImage = UIImage(contentsOfFile: filePath as String)!
//            
//            (cell.viewWithTag(1) as! UIImageView).image = image
//            
//        } else {
//            
//            Alamofire.request(house.awards[indexPath.row] as! String).responseData { response in
//                debugPrint("All Response Info: \(response)")
//                
//                if let data = response.result.value {
//                    
//                    do {
//                        try data.write(to: URL(fileURLWithPath: filePath as String))
//                    } catch {
//                        
//                    }
//                    
//                    let image:UIImage = UIImage(data: data)!//UIImage(contentsOfFile: filePath as String)!
//                    
//                    (cell.viewWithTag(1) as! UIImageView).image = image
//                    
//                }
//                
//            }

        
        return cell
    }
}
