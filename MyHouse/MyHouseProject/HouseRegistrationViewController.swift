//
//  HouseRegistrationViewController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 14.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SwiftValidator
import SwiftSpinner

class HouseRegistrationViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, ValidationDelegate, UITextFieldDelegate {
    @IBOutlet weak var CityTextField: UITextField!
    @IBOutlet weak var StreetTextField: UITextField!
    @IBOutlet weak var HouseTextField: UITextField!
    @IBOutlet weak var HelloTextField: UILabel!
    
    @IBOutlet weak var whiteBackgroundView: UIView!
    @IBOutlet weak var addHouseButton: UIButton!
    
    @IBOutlet weak var errorHouseLabel: UILabel!
    @IBOutlet weak var errorStreetLabel: UILabel!
    @IBOutlet weak var errorCityLabel: UILabel!
    
    
    var presenter:EDHouseRegistrationPresenter! = nil
    let validator = Validator()
    var pickerCity = UIPickerView()
    var pickerStreet = UIPickerView()
    var pickerHouse = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerCity.tag = 0
        pickerStreet.tag = 1
        pickerHouse.tag = 2
        pickerCity.delegate = self
        pickerStreet.delegate = self
        pickerHouse.delegate = self
        pickerCity.dataSource = self
        pickerStreet.dataSource = self
        pickerHouse.dataSource = self
        CityTextField.inputView = pickerCity

        whiteBackgroundView.layer.cornerRadius = 3.0
        addHouseButton.layer.cornerRadius = 3.0
        
        presenter = EDHouseRegistrationPresenter.init(view: self)

        validator.registerField(CityTextField, errorLabel: errorCityLabel, rules: [RequiredRule()])
        validator.registerField(StreetTextField, errorLabel: errorStreetLabel, rules: [RequiredRule()])
        
        StreetTextField.isEnabled = false
        HouseTextField.isEnabled = false
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "kEDAddressCityWasChanged"), object: nil, queue: OperationQueue.main) { (notification) in
            
            self.CityTextField.text = self.presenter.address.city.name
            self.StreetTextField.isEnabled = true
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "kEDAddressStreetWasChanged"), object: nil, queue: OperationQueue.main) { (notification) in
            
            if self.presenter.address.street != nil {
                self.StreetTextField.text = self.presenter.address.street.getFullAddress()
                self.HouseTextField.isEnabled = true
                
                SwiftSpinner.show("")
                
                self.presenter.loadHousesList(0)
                
            } else {
                self.HouseTextField.isEnabled = false
                self.StreetTextField.text = ""
            }
            
            
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "kEDAddressHouseWasChanged"), object: nil, queue: OperationQueue.main) { (notification) in
            
            if self.presenter.address.house != nil {
                self.HouseTextField.text = self.presenter.address.house.address
            } else {
                self.HouseTextField.text = ""
            }
            
            
        }

    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView.tag == 0 {
            return presenter.townsList.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            
            if presenter.townsList.count > row {
                presenter.address.city = presenter.townsList[row] as! EDCity
                CityTextField.text = (presenter.townsList[row] as? EDCity)?.name
            }
            
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return (presenter.townsList[row] as? EDCity)?.name
        } else{
            return "1"
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        

        if textField.tag == 2 {
            
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            
            let resultSearchVC:EDSearchHouseTableViewController = storyboard.instantiateViewController(withIdentifier: "searchHouse") as! EDSearchHouseTableViewController
            resultSearchVC.presenter = self.presenter
            resultSearchVC.presenter.search = resultSearchVC
            resultSearchVC.type = .street
            
            let searchVC:UISearchController = UISearchController(searchResultsController: resultSearchVC)
            searchVC.searchResultsUpdater = resultSearchVC
            searchVC.hidesNavigationBarDuringPresentation = true
            searchVC.dimsBackgroundDuringPresentation = true
            searchVC.searchBar.sizeToFit()
            searchVC.delegate = resultSearchVC
            
            self.present(searchVC, animated: true, completion: nil)
            
            
        }
        if textField.tag == 3 {
            
            if self.presenter.housesList.count > 0 {
                
                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                
                let resultSearchVC:EDSearchHouseTableViewController = storyboard.instantiateViewController(withIdentifier: "searchHouse") as! EDSearchHouseTableViewController
                resultSearchVC.presenter = self.presenter
                resultSearchVC.presenter.search = resultSearchVC
                resultSearchVC.type = .house
                
                let searchVC:UISearchController = UISearchController(searchResultsController: resultSearchVC)
                searchVC.searchResultsUpdater = resultSearchVC
                searchVC.hidesNavigationBarDuringPresentation = true
                searchVC.dimsBackgroundDuringPresentation = true
                searchVC.searchBar.sizeToFit()
                searchVC.searchBar.keyboardType = .numbersAndPunctuation
                //searchVC.searchBar.text = presenter.address.street.name + " "
                searchVC.delegate = resultSearchVC
                
                self.present(searchVC, animated: true, completion: nil)
                
            }

        }
        
    }
    
    @IBAction func HouseRegistrationButton(_ sender: AnyObject) {
        validator.validate(self)
    }
    
    @IBAction func addLaterAction(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "selectCity", sender: nil)
        
    }
    
    func validationSuccessful() {
        
        if presenter.address.house != nil {

            self.presenter.addHouse()
            errorHouseLabel.isHidden = true
            errorCityLabel.isHidden = true
            errorStreetLabel.isHidden = true

            StreetTextField.layer.borderWidth = 0
            HouseTextField.layer.borderWidth = 0
            CityTextField.layer.borderWidth = 0
            
        } else {
            
            performSegue(withIdentifier: "sendRequestForAddingAddress", sender: nil)
            
        }
    }

    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
        errorHouseLabel.isHidden = true
        errorCityLabel.isHidden = true
        errorStreetLabel.isHidden = true
        
        StreetTextField.layer.borderWidth = 0
        HouseTextField.layer.borderWidth = 0
        CityTextField.layer.borderWidth = 0
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }

            let textField = field as! UITextField
            
            if textField == CityTextField {
                
                error.errorLabel?.text = "Укажите ваш город"
                error.errorLabel?.isHidden = false
                
            } else if textField == StreetTextField {
                
                error.errorLabel?.text = "Укажите вашу улицу"
                error.errorLabel?.isHidden = false
                
            } else {
                
                error.errorLabel?.text = "Укажите номер вашего дома"
                error.errorLabel?.isHidden = false
                
            }

        }
        
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        if segue.identifier == "sendRequestForAddingAddress" {
            
            let dest:UINavigationController = segue.destination as! UINavigationController
            
            let dest_:EDRequestAddressViewController = (dest.viewControllers.first as? EDRequestAddressViewController)!
            
            dest_.addressStr = presenter.address.city.name + " " + presenter.address.street.getFullAddress() + " " + HouseTextField.text!
            
            dest_.address = presenter.address
            
        }
        
     }

}
