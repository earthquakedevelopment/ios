//
//  NewsViewController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 07.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeueCyr", size: 16)!]
    }
    
    @IBAction func SideMenuButton(_ sender: AnyObject) {
        self.slideMenuController()?.openRight()
    }
    
    
}
