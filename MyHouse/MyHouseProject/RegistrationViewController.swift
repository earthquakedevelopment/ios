//
//  RegistrationViewController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 15.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
//import PhoneNumberKit
import SwiftSpinner
import SwiftValidator

class RegistrationViewController: UIViewController, UITextFieldDelegate,ValidationDelegate {
    
    var presenter:EDRegistrationPresenter! = nil
    
    //let phoneNumberKit = PhoneNumberKit()
    let validator = Validator()
    
    @IBOutlet weak var errorNameLabel: UILabel!
    @IBOutlet weak var errorPhoneNumber: UILabel!
    @IBOutlet weak var errorPassLabel: UILabel!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var whiteBackgroundView: UIView!
    @IBOutlet weak var RegFIO: UITextField!
    @IBOutlet weak var RegSex: UISegmentedControl!
    @IBOutlet weak var RegPhone: UITextField!
    @IBOutlet weak var RegPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whiteBackgroundView.layer.cornerRadius = 3.0
        registrationButton.layer.cornerRadius = 3.0
        
        RegPhone.layer.cornerRadius = 3.0
        RegPhone.clipsToBounds = true
        
        RegFIO.layer.cornerRadius = 3.0
        RegFIO.clipsToBounds = true
        
        RegPass.layer.cornerRadius = 3.0
        RegPass.clipsToBounds = true
        
        presenter = EDRegistrationPresenter.init(view: self)
        
        validator.registerField(RegFIO, errorLabel: errorNameLabel, rules: [RequiredRule.init(message: "Необходимо ввести имя"), MinLengthRule.init(length: 3, message: "Введенное имя слишком короткое"), MaxLengthRule.init(length: 50, message: "Введеное имя слишком длинное")])
        validator.registerField(RegPhone, errorLabel: errorPhoneNumber, rules: [RequiredRule.init(message: "Необходимо ввести телефон")])
        validator.registerField(RegPass, errorLabel: errorPassLabel, rules: [RequiredRule.init(message: "Необходимо ввести пароль"), MinLengthRule.init(length: 6, message: "Введенный пароль слишком короткий"), MaxLengthRule.init(length: 50, message: "Введенный пароль слишком длинный")])

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func RegButton(_ sender: AnyObject) {
        
        validator.validate(self)
        
    }
    
    func textFieldFilter(_ textField: String) -> String {
        
        let aSet = CharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = textField.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return numberFiltered
        
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true);
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.init(red: 52.0/255.0, green: 168.0/255.0, blue: 83.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.borderWidth = 0
        
    }
    
    func validationSuccessful() {
        
        presenter.registrationUser(RegFIO.text!, password: RegPass.text!, phone: RegPhone.text!, gender: RegSex.selectedSegmentIndex)
        
        errorPassLabel.isHidden = true
        errorPhoneNumber.isHidden = true
        errorNameLabel.isHidden = true
        
        RegPass.layer.borderWidth = 0
        RegFIO.layer.borderWidth = 0
        RegPhone.layer.borderWidth = 0
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
        errorPassLabel.isHidden = true
        errorPhoneNumber.isHidden = true
        errorNameLabel.isHidden = true
        
        RegPass.layer.borderWidth = 0
        RegFIO.layer.borderWidth = 0
        RegPhone.layer.borderWidth = 0
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
            
            
            //            error.errorLabel?.text = error.errorMessage // works if you added labels
            //            error.errorLabel?.hidden = false
        }
        
    }

    @IBAction func showPolicyAction(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "policy", sender: nil)
        
    }

}
