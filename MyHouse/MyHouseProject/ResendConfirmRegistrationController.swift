//
//  ResendConfirmRegistrationController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 26.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import Foundation
import UIKit
import SwiftSpinner
import SwiftValidator

class ResendConfirmRegistrationViewController: UIViewController, UITextFieldDelegate, ValidationDelegate {
    
    @IBOutlet weak var errorCodeLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var ResendConfirmTextField: UILabel!
    @IBOutlet weak var ResendSMSLabel: UILabel!
    @IBOutlet weak var ResendSMSButton: UIButton!
    @IBOutlet weak var ResendCodeField: UITextField!
    
    let validator = Validator()
    var presenter:EDResendCodePresenter! = nil

    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var whiteBackgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whiteBackgroundView.layer.cornerRadius = 3.0
        confirmButton.layer.cornerRadius = 3.0
        
        ResendCodeField.layer.cornerRadius = 3.0
        ResendCodeField.clipsToBounds = true
        
        ResendSMSLabel.isHidden = true
        ResendSMSLabel.text = "Отправить смс через 5:00"
//        ResendConfirmTextField.text! = "СМС с кодом подтверждения было отправленно на номер:\n"+UserPhone
        
        phoneLabel.text = UserPhone
        presenter = EDResendCodePresenter.init(view: self)
        
        validator.registerField(ResendCodeField, errorLabel: errorCodeLabel, rules: [RequiredRule.init(message: "Введите код")])
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func checkCodeAction (_ sender: AnyObject) {
        
        validator.validate(self)
        

    }
    
    @IBAction func resendCodeAction (_ sender: AnyObject) {
        
        presenter.resendCodeAction()
        
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    internal func timerStarted() {
        
        ResendSMSButton.isHidden = true
        ResendSMSLabel.isHidden = false
        ResendSMSLabel.text = "Отправить смс через 5:00"
        
    }
    
    internal func timerFinished() {
        
        ResendSMSButton.isHidden = false
        ResendSMSLabel.isHidden = true
        ResendSMSLabel.text = "Отправить смс через 5:00"
        
    }

    internal func changeLeftTime (_ title: String) {
        
        ResendSMSLabel.text = title
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.init(red: 52.0/255.0, green: 168.0/255.0, blue: 83.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.borderWidth = 0
        
    }
    
    func validationSuccessful() {
        
        presenter.checkRegistrationCode(ResendCodeField.text!)
        
        
        errorCodeLabel.isHidden = true
        ResendCodeField.layer.borderWidth = 0

    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
        errorCodeLabel.isHidden = true
        ResendCodeField.layer.borderWidth = 0
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
            
        }
        
    }

}
