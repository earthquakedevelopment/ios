//
//  RightMenuViewController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 31.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

struct cellData{
    let cell: Int!
    let text: String!
    let image: UIImage!
    let type: UITableViewCellAccessoryType
}

class RightViewController : UITableViewController{
    
    var NameOfUser = "Константин Христорождественский"
    var UserAvavtarImage = UIImage(named: "Deineris")
    var CityOfUser = "Кострома"
    var arrayOfCellData = [cellData]()
    weak var slideViewController:SlideMenuController!
    
    override func viewDidLoad() {
        arrayOfCellData = [cellData(cell: 1, text: NameOfUser, image: UserAvavtarImage, type: .none),
                           cellData(cell: 2, text: CityOfUser, image: UIImage(named: ""), type: .none),
                           cellData(cell: 3, text: "Лента оповещений", image: UIImage(named: "menu_icon"), type: .none),
                           cellData(cell: 4, text: "Мои дома", image: UIImage(named: "message_icon"), type: .none),
                           cellData(cell: 5, text: "Настройки", image: UIImage(named: "cog_icon"), type: .disclosureIndicator),
                           cellData(cell: 6, text: "Выйти", image: UIImage(named: "icon_logout_white"), type: .none)]
        
        self.view.backgroundColor = UIColor(red: 53/255, green: 62/255, blue: 69/255, alpha: 1.0)
        }
    
    func changeViewController(_ itemId: String) {
        
        let destVC = storyboard!.instantiateViewController(withIdentifier: itemId)
        
        self.slideMenuController()?.changeMainViewController(destVC, close: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfCellData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrayOfCellData[(indexPath as NSIndexPath).row].cell == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserAvatarTableViewCell") as! UserAvatarTableViewCell
            
            cell.AvatarImageView!.image = arrayOfCellData[(indexPath as NSIndexPath).row].image
            cell.UserNameLabel!.setTitle(arrayOfCellData[(indexPath as NSIndexPath).row].text)
            cell.accessoryType = arrayOfCellData[(indexPath as NSIndexPath).row].type
            if (cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))){
                cell.separatorInset = UIEdgeInsets.zero
                cell.layoutMargins = UIEdgeInsets.zero
                cell.preservesSuperviewLayoutMargins = false
            }
            
            return cell
            
        } else if arrayOfCellData[(indexPath as NSIndexPath).row].cell == 2{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell") as! CityTableViewCell
            
            cell.CityNameLabel!.text = arrayOfCellData[(indexPath as NSIndexPath).row].text
            cell.accessoryType = arrayOfCellData[(indexPath as NSIndexPath).row].type
            if (cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))){
                cell.separatorInset = UIEdgeInsets.zero
                cell.layoutMargins = UIEdgeInsets.zero
                cell.preservesSuperviewLayoutMargins = false
            }
            
            return cell

        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "RightMenuCell", for: indexPath) as! EDMenuItemTableViewCell
            
            cell.titleLabel.text = arrayOfCellData[(indexPath as NSIndexPath).row].text
            cell.iconImageView.image = arrayOfCellData[(indexPath as NSIndexPath).row].image
            cell.accessoryType = arrayOfCellData[(indexPath as NSIndexPath).row].type
            
            if (cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))){
                cell.layoutMargins = UIEdgeInsets.zero
                cell.preservesSuperviewLayoutMargins = false
            }
            
            return cell
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrayOfCellData[(indexPath as NSIndexPath).row].cell == 1{
            
            return 166
            
        } else {
        
            return 44
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath as NSIndexPath).row {
        case 2:
        
            changeViewController("eventNC")
            
        case 3:
            
            changeViewController("Main")
            
        case 5:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: kEDRealtorNotificationKeyLogout), object: self)
            
        default: break
        }
        
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
        footerView.backgroundColor = UIColor(red: 53/255, green: 62/255, blue: 69/255, alpha: 1.0)
        
        return footerView
    }
    
    override func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        
        let cell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        
        switch (indexPath as NSIndexPath).row {
        case 5:
            
            cell.backgroundColor = UIColor(red: 223.0/255.0, green: 54.0/255.0, blue: 42.0/255.0, alpha: 1.0)
            
        case 1: break

        default:
            
            cell.backgroundColor = UIColor(red: 53.0/255.0, green: 62.0/255.0, blue: 69.0/255.0, alpha: 1.0)
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        
        let cell:UITableViewCell = tableView.cellForRow(at: indexPath)!

        cell.backgroundColor = UIColor(red: 59.0/255.0, green: 69.0/255.0, blue: 77.0/255.0, alpha: 1.0)

    }

}
