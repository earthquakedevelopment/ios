//
//  SlideMenuController.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 31.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ContainerViewController: SlideMenuController {
    
    override func awakeFromNib() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Main") {
            self.mainViewController = controller
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Right") {
            self.rightViewController = controller
        }
        super.awakeFromNib()
        
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: kEDRealtorNotificationKeyLogout), object: nil, queue: OperationQueue.main) { (notification) in
            
            self.navigationController?.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popToRootViewController(animated: true)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

}
