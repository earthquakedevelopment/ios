//
//  UserAvatarTableViewCell.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 02.09.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit

class UserAvatarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var BackgroundImageView: UIImageView!
    @IBOutlet weak var AvatarImageView: UIImageView!
    @IBOutlet weak var UserNameLabel: EDAttributedLabel!
    
    override func awakeFromNib() {
        
        let colorTop =  UIColor(red: 59.0/255.0, green: 69.0/255.0, blue: 77.0/255.0, alpha: 0).cgColor
        let colorBottom = UIColor(red: 59.0/255.0, green: 69.0/255.0, blue: 77.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.contentView.bounds
        
        self.contentView.layer.insertSublayer(gradientLayer, at: 1)
        
    }
    
    override func layoutSubviews() {
        AvatarImageView.layer.cornerRadius = AvatarImageView.frame.size.width/2
        AvatarImageView.clipsToBounds = true
        
        
    }
    
}
