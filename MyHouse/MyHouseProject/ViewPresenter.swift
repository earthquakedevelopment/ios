//
//  ViewPresenter.swift
//  MyHouseProject
//
//  Created by Юрий Солошенко on 14.08.16.
//  Copyright © 2016 Юрий Солошенко. All rights reserved.
//

import UIKit
import Alamofire

public var RegToken = "RegToken"
public var AppToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjYzNDksImlhdCI6MTQ3MTY0MDA1OSwiZXhwIjoxNDcyMjQ0ODU5fQ.VDTYRfTMmlZvJh5xFSXdlAdODH9Sc0XSR0Sr-GhyblY"
public var AuthStatus = "AuthStatus"
public var UserPhone = "Phone"
public var ResendCounter = 0
public var CityNames = ["String"]
public var CityCodes = ["String"]
public var CityRow = 0
public var StreetRow = 0
public var HouseID = 0
public var HouseCardID = "0"
public var CityData = ["ID": "CityName"]
public var StreetData = [["id": "4795591", "name": "Центральная", "socr": "пр-кт", "cid": "657", "City": "Кострома"]]
public var HouseData = [["address": "ул. Мира, д. 1", "avatar": "http://static.realtor.im/1461682821571f82853920d.png","City": "г. Кострома", "id": 7582688]]

//Функции для авторизации и регистрации в приложении
public func PhoneRegistration(RegName: String, RegSex: Int, RegPhone: String, RegPass: String){
    
    Alamofire.request(.POST, "http://api.realtor.im/v1/auth/signup", parameters: ["name": RegName, "sex":RegSex, "value":RegPhone, "password":RegPass])
        .responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                RegToken = "Bearer "+((JSON)["token"] as! String!)
            }
    }
}

public func ConfirmRegistration(RegToken: String, ConfirmCode: Double){
    
    Alamofire.request(.POST, "http://api.realtor.im/v1/auth/confirm", headers: ["Authorization": RegToken], parameters: ["code": ConfirmCode])
        .responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                AppToken = "Bearer "+((JSON)["token"] as! String!)
                print("AppToken: "+AppToken)
            }
    }
}


public func ResendSMS(RegToken: String, RegPhone: String){
    if(ResendCounter < 4)
    {
        Alamofire.request(.POST, "http://api.realtor.im/v1/auth/resend", headers: ["Authorization": RegToken], parameters: ["phone":RegPhone])
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
        }
    }
}

public func LoadCityList(AppToken: String){
    Alamofire.request(.GET, "http://api.realtor.im/v1/location/cities", headers: ["Authorization": AppToken])
        .responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                CityData = ((JSON)["cities"] as! NSDictionary!) as! [String : String]
                for(ID, CityName) in CityData
                {
                    print("\(ID): \(CityName)")
                }
                CityNames = [String](CityData.values)
                CityCodes = [String](CityData.keys)
                print(CityData)
                print(CityCodes)
                print(CityNames)
            }
    }
}

public func LoadStreetList(AppToken: String, IDOfCity: String, StreetName: String){
    print(IDOfCity)
    print(StreetName)
    let GetStreetURL = String("http://api.realtor.im/v1/location/streets?city="+IDOfCity+"&street="+StreetName)
    let SafeGetStreetUrl = GetStreetURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
    print(GetStreetURL)
    Alamofire.request(.GET, SafeGetStreetUrl!, headers: ["Authorization": AppToken])
        .responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                StreetData = ((JSON)["locations"] as! NSArray) as! [Dictionary<String, String>]
                print(StreetData)
            }
    }
    
}

public func LoadHouseList(AppToken: String, IDOfCity: String, StreetName: String){
    let GetHouseURL = String("http://api.realtor.im/v1/houses/search?city="+IDOfCity+"&q="+StreetName)
    let SafeGetHouseUrl = GetHouseURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
    print(GetHouseURL)
    Alamofire.request(.GET, SafeGetHouseUrl!, headers: ["Authorization": AppToken])
        .responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                let HouseCount = ((JSON)["count"] as! String)
                if HouseCount == "0" {
                    let alert = UIAlertController(title: "Ошибка", message: "На данной улице домов не найдено", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Oк", style: UIAlertActionStyle.Default, handler: nil))
                    HouseData = [["address": "", "avatar": "","City": "", "id": 0]]
                }
                else{
                    HouseData = ((JSON)["houses"] as! NSArray) as! [Dictionary<String, NSObject>]
                }
                print(HouseData)
            }
    }
}

public func HouseRegistraton(AppToken: String, HouseID: NSNumber){
    Alamofire.request(.POST, "http://api.realtor.im/v1/users/objects", headers: ["Authorization": AppToken], parameters: ["houseId": HouseID])
        .responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                HouseCardID = ((JSON)["cardId"] as! String)
            }
    }
}
//Функции для работы с приложением